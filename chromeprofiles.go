// Copyright (c) 2014 OSD. All rights reserved.
// This Source Code Form is subject to the terms of the BSD 2-Clause license.
// If a copy of the BSD license was not distributed with this file, you can
// obtain one at http://opensource.org/licenses/BSD-2-Clause.

package main

import (
	"bitbucket.org/osddk/go-ini"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"regexp"
	"sort"
)

const chromeLocalState = ".config/google-chrome/Local State"
const chromeDesktop = "/usr/share/applications/google-chrome.desktop"
const chromeDesktopLocal = ".local/share/applications/google-chrome.desktop"

type ChromeLocalStateProfile struct {
	Name     string `json:"name"`
	UserName string `json:"user_name"`
}

type ChromeLocalState struct {
	Profiles struct {
		Profile         map[string]ChromeLocalStateProfile `json:"info_cache"`
		ProfilesCreated int                                `json:"profiles_created"`
	} `json:"profile"`
}

type Profile struct {
	Name string
	Path string
}

type ProfileByName []Profile

func (a ProfileByName) Len() int           { return len(a) }
func (a ProfileByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ProfileByName) Less(i, j int) bool { return a[i].Name < a[j].Name }

func main() {
	u, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	path := filepath.Join(u.HomeDir, chromeLocalState)
	log.Println("Reading Chrome local state from", path)
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	v := new(ChromeLocalState)
	decoder := json.NewDecoder(file)
	if err := decoder.Decode(v); err != nil {
		log.Fatal(err)
	}

	log.Println("Reading original desktop file", chromeDesktop)
	ini, err := ini.ParseFile(chromeDesktop)
	if err != nil {
		log.Fatal(err)
	}

	// Build array of profiles for easier sorting.
	var profiles []Profile
	for path, profile := range v.Profiles.Profile {
		profiles = append(profiles, Profile{Name: profile.Name, Path: path})
	}
	sort.Sort(ProfileByName(profiles))

	// Add profiles to ini.
	re := regexp.MustCompile("[^a-zA-Z0-9]+")
	for _, profile := range profiles {
		log.Println("Adding profile:", profile.Name)
		action := fmt.Sprintf("New%sProfile", re.ReplaceAllLiteralString(profile.Path, ""))
		section := "Desktop Action " + action
		ini["Desktop Entry"]["Actions"] += action + ";"
		ini[section] = make(map[string]string)
		ini[section]["Name"] = "New " + profile.Name + " Window"
		ini[section]["Exec"] = ini["Desktop Entry"]["Exec"] + " --profile-directory=\"" + profile.Path + "\""
	}

	path = filepath.Join(u.HomeDir, chromeDesktopLocal)
	log.Println("Writing new desktop file", path)
	if err := ini.Write(path); err != nil {
		log.Fatal(err)
	}

	// Refresh desktop menu.
	log.Println("Refreshing desktop menu")
	cmd := exec.Command("xdg-desktop-menu", "forceupdate")
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}
