# Chrome profile desktop generator

Introduction
------------

This tool attempts to detect all Google Chrome profiles and add them to the
Google Chrome desktop file allowing you to open a new browser window with a
specific profile.

Note: It has only been tested with Go 1.2 on Ubuntu 13.10.


Usage
-----

To get and run the tool make sure GOPATH is set and run the following commands:

    go get bitbucket.org/osddk/chromeprofiles
    $GOPATH/bin/chromeprofiles
